#!/usr/bin/env bash

whats-on-port() {
  echo `sudo lsof -i :$1`
}

whats-my-internal-ip() {
  echo `ipconfig getifaddr en0`
}

whats-my-external-ip() {
  echo `curl -s ipinfo.io/ip`
}