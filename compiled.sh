##############################################
####### Compiled @ 1506711197.08 ####### 
##############################################
export DOTFILE_DIR='/Users/dt76278/.dotfiles'

##############################################
####### manifest.sh ####### 
##############################################

##############################################
####### /personal/config.sh ####### 
##############################################
export DEFAULT_EDITOR=nano

##############################################
####### /10-environment.sh ####### 
##############################################
# Performs a `ls -a` when using `cd ...`
chpwd() {
  emulate -L zsh
  ls -a
}
# Make directory and cd to it (dir)
mk() {
  mkdir "$1" && cd "$1"
}
# Easy delete dir/files (dir)
del() {
  rm -fr "$1"
}
# Clipboard alias
alias cb="pbcopy"
export PATH="/usr/local/sbin:$PATH"
export PATH="/usr/local/opt/python/libexec/bin:$PATH"
alias brewup='brew update; brew upgrade; brew prune; brew cleanup; brew doctor'
export PROMPT_EOL_MARK=""
export PATH="${HOME}/bin:$PATH"
alias t=todolist
cdf() {
    target=`osascript -e 'tell application "Finder" to if (count of Finder windows) > 0 then get POSIX path of (target of front Finder window as text)'`
    if [ "$target" != "" ]; then
        cd "$target"
    else
        echo 'No Finder window found' >&2
    fi
}
catc() {
    local out colored
    out=$(/bin/cat $@)
    colored=$(echo $out | pygmentize -f console -g 2>/dev/null)
    [[ -n $colored ]] && echo "$colored" || echo "$out"
}
answer_is_yes() {
  [[ "$REPLY" =~ ^[Yy]$ ]] && return 0 || return 1
}
ask() {
  print_question "$1"
  read
}
ask_for_confirmation() {
  print_question "$1 (y/n) "
  read -n 1
  printf "\n"
}
ask_for_sudo() {
  # Ask for the administrator password upfront
  sudo -v
  # Update existing `sudo` time stamp until this script has finished
  # https://gist.github.com/cowboy/3118588
  while true; do
    sudo -n true
    sleep 60
    kill -0 "$$" || exit
  done &> /dev/null &
}
cmd_exists() {
  [ -x "$(command -v "$1")" ] && printf 0 || printf 1
}
execute() {
  $1 &> /dev/null
  print_result $? "${2:-$1}"
}
get_answer() {
  printf "$REPLY"
}
get_os() {
  declare -r OS_NAME="$(uname -s)"
  local os=""
  if [ "$OS_NAME" == "Darwin" ]; then
    os="osx"
  elif [ "$OS_NAME" == "Linux" ] && [ -e "/etc/lsb-release" ]; then
    os="ubuntu"
  fi
  printf "%s" "$os"
}
is_git_repository() {
  [ "$(git rev-parse &>/dev/null; printf $?)" -eq 0 ] && return 0 || return 1
}
mkd() {
  if [ -n "$1" ]; then
    if [ -e "$1" ]; then
      if [ ! -d "$1" ]; then
        print_error "$1 - a file with the same name already exists!"
      else
        print_success "$1"
      fi
    else
      execute "mkdir -p $1" "$1"
    fi
  fi
}
print_error() {
  # Print output in red
  printf "\e[0;31m  [✖] $1 $2\e[0m\n"
}
print_info() {
  # Print output in purple
  printf "\n\e[0;35m $1\e[0m\n\n"
}
print_question() {
  # Print output in yellow
  printf "\e[0;33m  [?] $1 \e[0m"
}
print_result() {
  [ $1 -eq 0 ] && print_success "$2" || print_error "$2"
  [ "$3" == "true" ] && [ $1 -ne 0 ] && exit
}
print_success() {
  # Print output in green
  printf "\e[0;32m  [✔] $1\e[0m\n"
}

##############################################
####### /30-utilities.sh ####### 
##############################################

##############################################
####### /utilities/jump-servers.sh ####### 
##############################################
export SSH_JUMP_SERVER1=uxpapjmpwc01.dstcorp.net
export SSH_JUMP_SERVER2=uxpapjmpwc02.dstcorp.net
export SSH_JUMP_SERVER3=uxpapjmpic01.dstcorp.net
alias jump='ssh $SSH_JUMP_SERVER1'
alias jump-backup='ssh $SSH_JUMP_SERVER2'
# Send file to jump server (local file, remote file)
jump-send() {
  local DIR="~/"$2
  : ${DIR:="~"}
  scp "$1" $USER@$SSH_JUMP_SERVER1:$DIR
}

##############################################
####### /utilities/lab.sh ####### 
##############################################
alias lab-docker='ssh ELAB+${USER}@10.192.133.79'

##############################################
####### /utilities/password.sh ####### 
##############################################
# to get user password, used in other .dotfiles
get-password() {
  echo "$(security find-internet-password -a ${USER} -s dstproxy.dstcorp.net -w)"
}

##############################################
####### /utilities/vars.sh ####### 
##############################################
# Set all [array] to `value`
# Example: set_all arr "$value"
set-all() {
    name=$1[@]
    value=$2
    vars=("${!name}")
    for i in "${vars[@]}" ; do
      export $i="${value}"
    done
}
urlencode() {
  python -c 'import urllib, sys; print urllib.quote(sys.argv[1], sys.argv[2])' \
    "$1" "$urlencode_safe"
}

##############################################
####### /50-network.sh ####### 
##############################################

##############################################
####### /network/10-networksetup.sh ####### 
##############################################
# Switch network locations
network() {
  sudo networksetup -switchtolocation "$1"
}
alias network-dst='network DST'
alias network-home'network Home'
# Retrieve the current active network service
get-current-network-service() {
  services=$(networksetup -listnetworkserviceorder | grep 'Hardware Port')
  while read line; do
      sname=$(echo $line | awk -F  "(, )|(: )|[)]" '{print $2}')
      sdev=$(echo $line | awk -F  "(, )|(: )|[)]" '{print $4}')
      #echo "Current service: $sname, $sdev, $currentservice"
      if [ -n "$sdev" ]; then
          ifconfig $sdev 2>/dev/null | grep 'status: active' > /dev/null 2>&1
          rc="$?"
          if [ "$rc" -eq 0 ]; then
              currentservice="$sname"
          fi
      fi
  done <<< "$(echo "$services")"
  if [ -n $currentservice ]; then
      echo $currentservice
  else
      >&2 echo "Could not find current service"
      exit 1
  fi
}

##############################################
####### /network/20-proxy.sh ####### 
##############################################
export PROXY_ADDR="dstproxy.dstcorp.net:9119"
get-no-proxy-list() {
  networksetup -getproxybypassdomains "`get-current-network-service`" |
  while read interface; do
    local no_proxies="$no_proxies, $interface"
  done
  echo $no_proxies | cut -c 3-
}
get-proxy() {
  echo "http://${USER}:$(urlencode `get-password`)@${PROXY_ADDR}"
}
# Proxy managment functions
proxy-on() {
    export http_proxy="`get-proxy`"
    export https_proxy="${http_proxy}"
    export no_proxy="`get-no-proxy-list`"
    export HTTP_PROXY="${http_proxy}"
    export HTTPS_PROXY="${https_proxy}"
    export NO_PROXY="${no_proxy}"
    proxy-status
}
proxy-off() {
    unset http_proxy https_proxy no_proxy HTTP_PROXY HTTPS_PROXY NO_PROXY
    proxy-status
}
proxy-status() {
    if [ -z ${http_proxy+x} ]; then
        printf "DST proxy is disabled.\n"
    else
        printf "DST proxy is enabled.\n"
    fi
}

##############################################
####### /network/misc.sh ####### 
##############################################
whats-on-port() {
  sudo lsof -i :$1
}
if [[ "$(networksetup -getcurrentlocation)" = "DST" ]]; then proxy-on; fi

##############################################
####### /70-third-party.sh ####### 
##############################################

##############################################
####### /third-party/apache.sh ####### 
##############################################
######################################
####### Apache #######################
######################################
alias apache-conf='sudo $DEFAULT_EDITOR /etc/apache2/users/${USER}.conf'
alias apache-on='sudo /usr/sbin/apachectl start'
alias apache-off='sudo /usr/sbin/apachectl stop'
alias apache-restart='sudo /usr/sbin/apachectl restart'

##############################################
####### /third-party/dnsmasq.sh ####### 
##############################################
######################################
####### DNS ##########################
######################################
dns-conf() {
  $DEFAULT_EDITOR /usr/local/etc/dnsmasq.conf
}
dns-on() { sudo brew services start dnsmasq }
dns-off() { sudo brew services stop dnsmasq }
dns-restart() {
  dns-off
  dns-on
}

##############################################
####### /third-party/docker.sh ####### 
##############################################
export ARTIFACTORY_DOCKER_ALL=artifacts-scm.dstcorp.net:9001
export ARTIFACTORY_DOCKER_HUB=artifacts-scm.dstcorp.net:9002
export ARTIFACTORY_DOCKER_LOCAL=artifacts-scm.dstcorp.net:9003
export ARTIFACTORY_DOCKER_REDHAT=artifacts-scm.dstcorp.net:9004
export DOCKER_PREFERRED_MIRROR=$ARTIFACTORY_DOCKER_ALL
# Clean up stopped containers
alias d-prune='docker system prune -f'
# Kill and clean up all containers
alias d-kill='docker stop $(docker ps -a -q) && d-prune'
# Short-hand list all containers
alias d-ps='docker ps -a'
# Easy pull through artifactory and retag as desired name
d-pull() {
  docker pull $DOCKER_PREFERRED_MIRROR/$1
  docker tag $DOCKER_PREFERRED_MIRROR/$1 $1
  docker rmi $DOCKER_PREFERRED_MIRROR/$1
}
#####################################
# --- Latest Container commands --- #
# Remote into the latest container
alias d-rsh='docker exec -it $(docker ps -lq) bash'
# Follow logs of latest container
alias d-logs='docker logs -f $(docker ps -lq)'
# Top in latest container
alias d-top='docker top $(docker ps -lq)'

##############################################
####### /third-party/docker/mysql.sh ####### 
##############################################
export MYSQL_ROOT_PASSWORD=password
##
# Start a simple MySQL server
# $1 - Name of docker image
# $2 - SQL init script
#
d-mysql() {
  docker run \
    -p 3306:3306 \
    -e VIRTUAL_HOST='db.localhost.dstcorp.net' \
    -e VIRTUAL_PORT=3306 \
    --name $1 \
    -v $(pwd)/$2:/docker-entrypoint-initdb.d/init.sql \
    -e MYSQL_ROOT_PASSWORD=$MYSQL_ROOT_PASSWORD \
    -d DOCKER_PREFERRED_MIRROR/mysql:latest
}
d-mysql-connect() {
  docker exec -it $1 bash -l -c 'mysql -uroot -p$MYSQL_ROOT_PASSWORD'
}

##############################################
####### /third-party/docker/nginx.sh ####### 
##############################################
d-nginx() {
  docker run -d \
    --name nginx-proxy \
    -p 80:80 \
    -v /var/run/docker.sock:/tmp/docker.sock:ro \
    $DOCKER_PREFERRED_MIRROR/jwilder/nginx-proxy
}
alias d-proxy="docker start nginx-proxy"

##############################################
####### /third-party/git.sh ####### 
##############################################
# Easy utilities for Git
# Open current directory project in browser
git-ui() {
  UI_URL=`git config --get remote.origin.url | sed "s/.*\@\(.*\):\(.*\)\..*/\1\/\2\/tree\//"`
  open http://$UI_URL`git branch | cut -d' ' -f 2`
}

##############################################
####### /third-party/nginx.sh ####### 
##############################################
####### NGINX ########################
alias nginx-servers='cd /usr/local/etc/nginx/servers'
alias nginx-on='sudo nginx'
alias nginx-off='sudo nginx -s stop'
alias nginx-restart='sudo nginx -s reload'
# Add a server easily based on a template
nginx-add-server() {
  sed -e "s/\${domain}/$1/" -e "s/\${port}/$2/" <<< "`curl -s https://raw.githubusercontent.com/Larchy/nginx-utils/master/server-template.conf`" > /usr/local/etc/nginx/servers/$1.conf
}

##############################################
####### /third-party/openshift.sh ####### 
##############################################
export OC_CLUSTER_SANDBOX=sandbox.cap.dstcorp.net
export OC_CLUSTER_DEV=dev.cap.dstcorp.net
export OC_CLUSTER_EAP=eap.cap.dstcorp.net
export OC_CLUSTER_PROD=prod.cap.dstcorp.net
# Construct OpenShift URL
oc-url() { echo https://openshift.$1:8443 }
# Reuse function to log in accepting $1=OpenShift URI
oc-login() {
  proxy_off
  oc login `oc-url $1` --username=${USER} --password=$(get_password)
  docker login -u $(oc whoami) -p $(oc whoami -t) docker-registry-default.$1
}
# Launch Browser for OpenShift UI
oc-ui() { open `oc-url $1` }
# Log in implementations
alias oc-sandbox='oc-login $OC_CLUSTER_SANDBOX'
alias oc-sandbox-ui='oc-ui $OC_CLUSTER_SANDBOX'
alias oc-dev='oc-login $OC_CLUSTER_DEV'
alias oc-dev-ui='oc-ui $OC_CLUSTER_DEV'
alias oc-eap='oc-login $OC_CLUSTER_EAP'
alias oc-eap-ui='oc-ui $OC_CLUSTER_EAP'
alias oc-prod='oc-login $OC_CLUSTER_PROD'
alias oc-prod-ui='oc-ui $OC_CLUSTER_PROD'

##############################################
####### /90-personal.sh ####### 
##############################################

##############################################
####### /personal/admin.sh ####### 
##############################################
alias admin='sudo /usr/bin/dscl . -append /Groups/admin GroupMembership $(/usr/bin/logname)'

##############################################
####### /personal/alias.sh ####### 
##############################################
alias dev='cd ~/dev'

##############################################
####### /personal/config.sh ####### 
##############################################
export DEFAULT_EDITOR=nano

##############################################
####### /99-scratch-pad.sh ####### 
##############################################
# Scratch pad for temporarily storing and building new .dotfiles
alias_test() {
  #!/usr/bin/env bash
  ALIAS_FILE=$HOME/.alias
  if [ ! -f $ALIAS_FILE ]; then
    /usr/bin/printf "Lazy creating ${ALIAS_FILE}.\nBe sure to source it in your shell profile:\n    source ${ALIAS_FILE}"
    touch $ALIAS_FILE
  fi
  if grep -q "$1" "$ALIAS_FILE"; then
    /usr/bin/printf "Alias section for '$1' already exists. Please remove and re-run or manually update them with the contents of:\n    $2"
  else
    ALIAS_CONTENT=`curl -s "$2"`
    /usr/bin/printf "\n\n### $1 ###########\n$ALIAS_CONTENT" >> $ALIAS_FILE
  fi
}
