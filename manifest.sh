#!/usr/bin/env bash

source $DOTFILE_DIR/personal/config.sh
source $DOTFILE_DIR/10-environment.sh
source $DOTFILE_DIR/30-utilities.sh
source $DOTFILE_DIR/50-network.sh
source $DOTFILE_DIR/70-third-party.sh
source $DOTFILE_DIR/90-personal.sh
source $DOTFILE_DIR/99-scratch-pad.sh
