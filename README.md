DOTFILES
--------

Some useful bash shortcuts, commands, and aliases.

### Installation
Clone this repo to `~/.dotfiles` directory:

`git clone git@gitlab.com:larchy/dotfiles.git ~/.dotfiles`

Then import `~/.dotfiles/index.sh` - recommend adding to `~/.bashrc` and if you use ZSH, import `~/.bashrc` into `~/.zshrc`.

```bash
source ~/.dotfiles/index.sh
```
