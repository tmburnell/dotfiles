#! /usr/bin/python
import os
import re
import time

def createTitle(title):
    return "##############################################\n\
####### " + title + " ####### \n\
##############################################\n"

def processFile(fileName):
    temp = "\n" + createTitle(fileName.replace(DOTFILE_DIR, ""))

    with open(fileName) as f:
        for line in f:
            if not re.match('#.* bash', line) \
               and line.strip():

                toSource = re.split('^source (.*?)$', line)
                if len(toSource) > 1:
                    temp += processFile(toSource[1].replace("$DOTFILE_DIR", DOTFILE_DIR))
                else:
                    toSource = re.split('^source-all (.*?)$', line)

                    if len(toSource) > 1:
                        sdir = toSource[1].replace("$DOTFILE_DIR", DOTFILE_DIR)
                        f = []
                        for (dirpath, dirnames, filenames) in os.walk(sdir):
                            f.extend(filenames)
                            for s in f:
                                temp += processFile(sdir + "/" + s)
                            break
                    else:
                        temp += line

    return temp

DOTFILE_DIR = os.path.dirname(os.path.realpath(__file__))
output = createTitle("Compiled @ " + str(time.time()))
output += "export DOTFILE_DIR='" + DOTFILE_DIR + "'\n"
output += processFile('manifest.sh')

with open(DOTFILE_DIR + "/compiled.sh", 'w') as f:
    f.write(output)
    f.close()
