#!/usr/bin/env bash

######################################
####### DNS ##########################
######################################
dns-conf() {
  $DEFAULT_EDITOR /usr/local/etc/dnsmasq.conf
}
dns-on() { sudo brew services start dnsmasq }
dns-off() { sudo brew services stop dnsmasq }
dns-restart() {
  dns-off
  dns-on
}
