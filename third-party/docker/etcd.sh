#!/bin/bash
d-etcd-create() {
    NAME=${1:-"etcd"}

    # Lazy create docker volume
    docker volume ls | grep -q $NAME-data && echo "Using $NAME-data volume" || docker volume create --name $NAME-data

    docker run \
        -p 2379:2379 \
        -p 2380:2380 \
        -d \
        --volume=$NAME-data:/$NAME-data \
        --name $NAME gcr.io/etcd-development/etcd:latest \
        /usr/local/bin/etcd \
        --data-dir=/$NAME-data --name $NAME \
        --initial-advertise-peer-urls http://127.0.0.1:2380 --listen-peer-urls http://0.0.0.0:2380 \
        --advertise-client-urls http://127.0.0.1:2379 --listen-client-urls http://0.0.0.0:2379 \
        --initial-cluster $NAME=http://127.0.0.1:2380
}

d-etcd-start() {
    docker start etcd >> /dev/null
    echo "etcd started!"
}