#!/usr/bin/env bash

export MYSQL_ROOT_PASSWORD=password

##
# Start a simple MySQL server
# $1 - Name of docker image
# $2 - SQL init script
#
d-mysql() {
  docker run \
    -p 3306:3306 \
    -e VIRTUAL_HOST='db.localhost.dstcorp.net' \
    -e VIRTUAL_PORT=3306 \
    --name $1 \
    -v $(pwd)/$2:/docker-entrypoint-initdb.d/init.sql \
    -e MYSQL_ROOT_PASSWORD=$MYSQL_ROOT_PASSWORD \
    -d DOCKER_PREFERRED_MIRROR/mysql:latest
}

d-mysql-connect() {
  docker exec -it $1 bash -l -c 'mysql -uroot -p$MYSQL_ROOT_PASSWORD'
}
