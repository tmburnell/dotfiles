#!/bin/bash

# Easy utilities for Git

# Open current directory project in browser
git-ui() {
  UI_URL=`git config --get remote.origin.url | sed "s/.*\@\(.*\):\(.*\)\..*/\1\/\2\/tree\//"`
  open http://$UI_URL`git branch | cut -d' ' -f 2`
}

# git-clone {repo} [base dir]
git-clone() {
  REPO=$1
  BASE_DIR=${2:-~}
  HTTPS=false

  if [ -z `echo $REPO | sed -n "/^https:/p"` ]
  then
    NS=`echo $REPO | sed "s/.*\@\(.*\):\(.*\)\/\(.*\)\..*/\1\/\2/"`
  else
    NS=`echo $REPO | sed "s/https:\/\/\(.*\)\/\(.*\)\..*/\1/"`
    HTTPS=true
  fi

  mkdir -p $BASE_DIR/$NS
  cd $BASE_DIR/$NS
  git clone $REPO
  
  if $HTTPS
  then
    cd `echo $REPO | sed "s/https:\/\/\(.*\)\/\(.*\)\..*/\2/"`
  else
    cd `echo $REPO | sed "s/.*\@\(.*\)\..*:\(.*\)\/\(.*\)\..*/\3/"`
  fi
}

git-push() {
  MSG=${1:-"Quick Push"}
  git add . && git commit -m $MSG && git pull && git push
}

git-update() {
  for REPO in `find . -name '.git' -type d`
  do
    DIR=`echo $REPO | sed "s/\(.*\)\.git/\1/"`
    (cd $DIR >> /dev/null && echo "\nUpdating `pwd`" && git stash && git pull && git stash pop && echo "")
  done
}

# Create a new key, add it to the agent
# $EMAIL, $ALIAS
git-new-key() {
  if [[ -z $1 || -z $2 || -z $3 ]]; then
    echo "Email, Alias, and URL required\n    me@example.com example gitlab.com"
    return
  fi

  ssh-keygen -t rsa -b 4096 -C "$1" -N "" -f ~/.ssh/id_rsa_$2
  eval "$(ssh-agent -s)" >> /dev/null
  echo ""
  ssh-add ~/.ssh/id_rsa_$2

  cat <<EOF >> ~/.ssh/config

Host $2.$3
HostName $3
PreferredAuthentications publickey
IdentityFile ~/.ssh/id_rsa_$2

EOF

  pbcopy < ./id_rsa_$2.pub
  echo "\nPublic key copied to clipboard"
}

# git-aclone alias repo [base dir]
git-aclone() {
  if [[ -z $1 || -z $2 ]]; then
    echo "Repo required\n    git-aclone git@gitlab.com:group/repo.git [alias]\n    git-aclone https://gitlab.com/group/repo.git [email]\n      Optional 3rd parameter for base directory"
    return
  fi

  BASE_DIR=${3:-~}

  if [ -z `echo $1 | sed -n "/^https:/p"` ] ; then
    # SSH
    local -x RIGHT=${1#*git@}  # Right of git@
    local -x  HOST=${RIGHT%:*} # Left of :
    local -x    GR=${RIGHT#*:} # Right of :
    local -x   DIR=$BASE_DIR/$HOST/${GR%/*}
    local -x   END=`basename "$GR"`
    local -x  REPO=${END%.*}
    local -x CLONE=git@$2.$HOST:$GR
    local -x EMAIL=`cat ~/.ssh/id_rsa_$2.pub | sed "s/ssh-rsa .* \(.*\)/\1/"`
  else
    # HTTPS
    local -x RIGHT=${1#*https://}        # Right of https://
    local -x   DIR=$BASE_DIR/${RIGHT%/*} # Left of /*.git
    local -x   END=`basename "$RIGHT"`
    local -x  REPO=${END%.*}
    local -x CLONE=$1
    local -x EMAIL=$2
  fi

  mkdir -p $DIR && cd $DIR && git clone $CLONE && cd $REPO && git config user.email $EMAIL
}