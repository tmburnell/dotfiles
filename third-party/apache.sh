#!/usr/bin/env bash

######################################
####### Apache #######################
######################################
alias apache-conf='sudo $DEFAULT_EDITOR /etc/apache2/users/${USER}.conf'
alias apache-on='sudo /usr/sbin/apachectl start'
alias apache-off='sudo /usr/sbin/apachectl stop'
alias apache-restart='sudo /usr/sbin/apachectl restart'
