#!/usr/bin/env bash

####### NGINX ########################
alias nginx-servers='cd /usr/local/etc/nginx/servers'
alias nginx-on='sudo nginx'
alias nginx-off='sudo nginx -s stop'
alias nginx-restart='sudo nginx -s reload'

# Add a server easily based on a template
nginx-add-server() {
  sed -e "s/\${domain}/$1/" -e "s/\${port}/$2/" <<< "`curl -s https://raw.githubusercontent.com/Larchy/nginx-utils/master/server-template.conf`" > /usr/local/etc/nginx/servers/$1.conf
}
