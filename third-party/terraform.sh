#!/usr/bin/env bash

####### Terraform Alias ########################
alias t-graph='terraform graph | dot -Tpng | open -a Preview -f'
alias t-init='terraform init'
alias t-plan='terraform plan'
alias t-apply='terraform apply'
