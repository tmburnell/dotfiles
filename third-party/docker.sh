#!/usr/bin/env bash

export ARTIFACTORY_DOCKER_ALL=artifacts-scm.dstcorp.net:9001
export ARTIFACTORY_DOCKER_HUB=artifacts-scm.dstcorp.net:9002
export ARTIFACTORY_DOCKER_LOCAL=artifacts-scm.dstcorp.net:9003
export ARTIFACTORY_DOCKER_REDHAT=artifacts-scm.dstcorp.net:9004

export DOCKER_PREFERRED_MIRROR=$ARTIFACTORY_DOCKER_ALL

# Clean up stopped containers
alias d-prune='docker system prune -f'

# Kill and clean up all containers
alias d-kill='docker stop $(docker ps -a -q) && d-prune'

# Short-hand list all containers
alias d-ps='docker ps -a'

# Easy pull through artifactory and retag as desired name
d-pull() {
  docker pull $DOCKER_PREFERRED_MIRROR/$1
  docker tag $DOCKER_PREFERRED_MIRROR/$1 $1
  docker rmi $DOCKER_PREFERRED_MIRROR/$1
}

#####################################
# --- Latest Container commands --- #

# Remote into the latest container
alias d-rsh='docker exec -it $(docker ps -lq) /bin/bash'

# Follow logs of latest container
alias d-logs='docker logs -f $(docker ps -lq)'

# Top in latest container
alias d-top='docker top $(docker ps -lq)'

# Stop all containers
alias d-stop='docker stop $(docker ps -q)'

source-all $DOTFILE_DIR/third-party/docker
