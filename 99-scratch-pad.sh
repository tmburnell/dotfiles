#!/usr/bin/env bash

# Scratch pad for temporarily storing and building new .dotfiles

alias_test() {
  #!/usr/bin/env bash

  ALIAS_FILE=$HOME/.alias

  if [ ! -f $ALIAS_FILE ]; then
    /usr/bin/printf "Lazy creating ${ALIAS_FILE}.\nBe sure to source it in your shell profile:\n    source ${ALIAS_FILE}"
    touch $ALIAS_FILE
  fi

  if grep -q "$1" "$ALIAS_FILE"; then
    /usr/bin/printf "Alias section for '$1' already exists. Please remove and re-run or manually update them with the contents of:\n    $2"
  else
    ALIAS_CONTENT=`curl -s "$2"`
    /usr/bin/printf "\n\n### $1 ###########\n$ALIAS_CONTENT" >> $ALIAS_FILE
  fi
}
