#!/bind/bash

health-check() {
    ENDPOINT=$1
    if [ $ENDPOINT = "" ]; then
        echo "No endpoint given!"
    fi

    WAIT=${2:-1}
    ATTEMPTS=${3:-10}
    
    for ((n=0;n<$ATTEMPTS;n++)) 
    do
        if [ $n -gt 0 ]; then sleep $WAIT; fi
        response=`curl --write-out '%{http_code}' --silent --output /dev/null $ENDPOINT`
        if [ $response -eq 200 ]; then return 0; fi
    done

    return 1
}