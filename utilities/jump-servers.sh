#!/usr/bin/env bash

export SSH_JUMP_SERVER1=uxpapjmpwc01.dstcorp.net
export SSH_JUMP_SERVER2=uxpapjmpwc02.dstcorp.net
export SSH_JUMP_SERVER3=uxpapjmpic01.dstcorp.net

alias jump='ssh $SSH_JUMP_SERVER1'
alias jump-backup='ssh $SSH_JUMP_SERVER2'

# Send file to jump server (local file, remote file)
jump-send() {
  local DIR="~/"$2
  : ${DIR:="~"}
  scp "$1" $USER@$SSH_JUMP_SERVER1:$DIR
}
